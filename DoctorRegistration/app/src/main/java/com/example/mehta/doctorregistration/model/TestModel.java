package com.example.mehta.doctorregistration.model;

/**
 * Created by adminpc on 4/5/18.
 */

public class TestModel {
    private String firstText;
    private String secondText;
    private String thirdText;
    private String fourthText;

    public TestModel(String firstText, String secondText,String thirdText, String fourthText) {
        this.firstText = firstText;
        this.secondText = secondText;
        this.thirdText=thirdText;
        this.fourthText=fourthText;
    }

    public String getFirstText() { return firstText; }

    public void setFirstText(String firstText) {
        this.firstText = firstText;
    }

    public String getSecondText() { return secondText; }

    public void setSecondText(String secondText) {this.secondText = secondText; }

    public String getThirdTextText() { return thirdText; }

    public void setThirdText(String thirdText) {this.thirdText = thirdText; }

    public String getFourthText() { return fourthText; }

    public void setFourthText(String fourthText) {this.secondText = fourthText; }
}
