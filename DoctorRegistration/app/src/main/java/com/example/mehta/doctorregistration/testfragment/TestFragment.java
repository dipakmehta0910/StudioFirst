package com.example.mehta.doctorregistration.testfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mehta.doctorregistration.R;

/**
 * Created by adminpc on 9/5/18.
 */

public class TestFragment extends Fragment{  //suport fragment manager

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View mRoot = inflater.inflate(R.layout.test_fragment, container, false);
        return mRoot;
    }
}
