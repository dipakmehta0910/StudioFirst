package com.example.mehta.doctorregistration.testlist;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.mehta.doctorregistration.R;
import com.example.mehta.doctorregistration.model.TestModel;

import java.util.List;

/**
 * Created by adminpc on 4/5/18.
 */

public class TestBaseAdapter extends BaseAdapter{
    private List<TestModel>mTestModelList;
    LayoutInflater mLayoutInflater;
    private Activity mActivity;

    public TestBaseAdapter(Activity activity, List<TestModel>testModelList) {
        this.mTestModelList = testModelList;
        this.mActivity = activity;
        this.mLayoutInflater = LayoutInflater.from(mActivity);
    }

    @Override
    public int getCount() { return mTestModelList.size(); }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.list_row, viewGroup, false);
        }
        TextView rowFirstTV = view.findViewById(R.id.list_row_first);
        TextView rowSecondTV = view.findViewById(R.id.list_row_second);
        TextView rowThirdTV = view.findViewById(R.id.list_row_third);
        TextView rowFourthTV = view.findViewById(R.id.list_row_fourth);
        rowFirstTV.setText(mTestModelList.get(i).getFirstText());
        rowSecondTV.setText(mTestModelList.get(i).getSecondText());
        rowThirdTV.setText(mTestModelList.get(i).getThirdTextText());
        rowFourthTV.setText(mTestModelList.get(i).getFourthText());
        return view;
    }
}
