package com.example.mehta.doctorregistration.testlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mehta.doctorregistration.R;
import com.example.mehta.doctorregistration.TestGridActivity;
import com.example.mehta.doctorregistration.model.TestModel;
import com.example.mehta.doctorregistration.model.TestModelGrid;

import java.util.ArrayList;
import java.util.List;

public class TestGridAdapter extends BaseAdapter {
    private List<TestModelGrid>mtestModelGridList;
    LayoutInflater mLayoutInflater;
    private Activity mActivity;

    public TestGridAdapter(TestGridActivity testGridActivity,List<TestModelGrid>testModelGrids) {
        this.mtestModelGridList=testModelGrids;
      this.mActivity=testGridActivity;
        this.mLayoutInflater = LayoutInflater.from(mActivity);
    }

    @Override
    public int getCount() {
        return mtestModelGridList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.grid_row_column, viewGroup, false);
        }
        TextView rowFirstTV = view.findViewById(R.id.dr_varma);
        TestModelGrid testModelGrid=mtestModelGridList.get(i);
        String s=testModelGrid.getFourthtext();
        rowFirstTV.setText(s);
        return view;
    }
}
