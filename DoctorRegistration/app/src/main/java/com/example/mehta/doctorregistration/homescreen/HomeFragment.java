package com.example.mehta.doctorregistration.homescreen;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mehta.doctorregistration.R;

public class HomeFragment extends Fragment {

    @Override
    public void onAttach(Context context) { super.onAttach(context); }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
     View mRoot=inflater.inflate(R.layout.home_fragment,container,false);
     return mRoot;
    }
}
