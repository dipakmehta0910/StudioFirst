package com.example.mehta.doctorregistration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.mehta.doctorregistration.testfragment.TestFragmentActivity;


public class MainActivity extends AppCompatActivity {
    private Button mMainBttn;
    private Button mMainBttn2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("doctors onCreate");
      mMainBttn=(Button)findViewById(R.id.button);
      mMainBttn2=(Button)findViewById(R.id.button2);

      mMainBttn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent=new Intent(MainActivity.this,LoginActivityd.class);
              startActivity(intent);
          }
      });
      mMainBttn2.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent=new Intent(MainActivity.this,TestFragmentActivity.class);
              startActivity(intent);
          }
      });
    }
}
