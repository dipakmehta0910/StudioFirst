package com.example.mehta.doctorregistration;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.GridView;

import com.example.mehta.doctorregistration.model.TestModelGrid;
import com.example.mehta.doctorregistration.testlist.TestGridAdapter;

import java.util.ArrayList;

public class TestGridActivity extends Activity {
    private GridView mtestgrid;
    ArrayList<TestModelGrid> list=new ArrayList<TestModelGrid>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_view);
        mtestgrid=(GridView)findViewById(R.id.grid_view);
       dummylistgrid();
        TestGridAdapter testGridAdapter=new TestGridAdapter(this,list);
        mtestgrid.setAdapter(testGridAdapter);
    }

    //create the dummy list of TestModelGrid as much we need to display in grid view
    //We can create multiple refrence varriable with multiple object
    //or we can create one refrence varriable with multiple object as shown in commented one
    public void dummylistgrid(){
        TestModelGrid testModelGrid = new TestModelGrid("dr ssharma ");
        list.add(testModelGrid);
        TestModelGrid testModelGrid1 = new TestModelGrid("dr varma ");
        list.add(testModelGrid1);
        TestModelGrid testModelGrid2 = new TestModelGrid("dr kushal ");
        list.add(testModelGrid2);
        TestModelGrid testModelGrid3 = new TestModelGrid("dr ssingh ");
        list.add(testModelGrid3);
        TestModelGrid testModelGrid4 = new TestModelGrid("dr nayak ");
        list.add(testModelGrid4);
        TestModelGrid testModelGrid5 = new TestModelGrid("dr avasthi ");
        list.add(testModelGrid5);

        /*TestModelGrid testModelGrid = new TestModelGrid("dr ssharma ");
        list.add(testModelGrid);
        testModelGrid = new TestModelGrid("dr varma ");
        list.add(testModelGrid);
        testModelGrid = new TestModelGrid("dr kushal ");
        list.add(testModelGrid);
        testModelGrid = new TestModelGrid("dr ssingh ");
        list.add(testModelGrid);
        testModelGrid = new TestModelGrid("dr nayak ");
        list.add(testModelGrid);
        testModelGrid = new TestModelGrid("dr avasthi ");
        list.add(testModelGrid);*/
    }
}
