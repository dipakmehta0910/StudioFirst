package com.example.mehta.doctorregistration.homescreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.mehta.doctorregistration.R;

public class HomeFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_fragment_activity);
        replaceHomeFragmentinContainer();
    }
    private void replaceHomeFragmentinContainer(){
        HomeFragment homeFragment1=new HomeFragment();
        HomeFragment homeFragment2=new HomeFragment();

        FragmentTransaction fragmentTransaction11=getSupportFragmentManager().beginTransaction();
        FragmentTransaction fragmentTransaction22=getSupportFragmentManager().beginTransaction();

        fragmentTransaction11.add(R.id.homecontainer1,homeFragment1).addToBackStack(null).commit();


    }
}
