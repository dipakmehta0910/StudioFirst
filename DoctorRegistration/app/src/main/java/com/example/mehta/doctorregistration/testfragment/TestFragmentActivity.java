package com.example.mehta.doctorregistration.testfragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.mehta.doctorregistration.R;

/**
 * Created by adminpc on 9/5/18.
 */

public class TestFragmentActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_fragment_activity);
        replaceFragmentInContainer();
    }

    /**
     * this method is used for adding or replacing the fragment in container, which is placed in activity
     * fragmentTransaction.add (this method is used to fill the fragment in particular id container
     * Here its R.id.container which means frame layout container)
     * addToBackStack is required to add this transaction in back stack (from here if we will again navigate
     * to another fragment then on back press from this stack this fragment will popped out)
     * commit is required to apply the changes
     */

    private void replaceFragmentInContainer() {
        TestFragment testFragment1 = new TestFragment();
        TestFragment testFragment2 = new TestFragment();

        FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
        FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();

        fragmentTransaction1.add(R.id.container1, testFragment1).addToBackStack(null).commit();
        fragmentTransaction2.add(R.id.container2, testFragment2).addToBackStack(null).commit();
    }
}
