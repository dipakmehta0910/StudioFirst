package com.example.mehta.doctorregistration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.widget.ImageView;

public class SplashActivityy extends Activity {

    private static int SPLASH_TIME_OUT=3000;
    private Object mLockedObject = new Object();
    private ImageView mImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("Dipak kumar onCreate SplashActivityy");
        setContentView(R.layout.activity_splashh);
        //mImageView = findViewById(R.id.image);

        SeparateThread separateThread = new SeparateThread();
        separateThread.start();
    }

    private class SeparateThread extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                Thread.sleep(SPLASH_TIME_OUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally {
                navigateToNextActivityUsingMainThread();
            }
        }
    }

    private void navigateToNextActivityUsingMainThread() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashActivityy.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
        protected void onStart() {
            super.onStart();
            System.out.println("Dipak kumar on start SplashActivityy");
        }

        @Override
        protected void onResume() {
            super.onResume();
            System.out.println("Dipak kumar on resume SplashActivityy");
        }

        @Override
        protected void onPause() {
            super.onPause();
            System.out.println("Dipak kumar on pause SplashActivityy");
        }

        @Override
        protected void onStop() {
            super.onStop();
            System.out.println("Dipak kumar on stop SplashActivityy");
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();
            System.out.println("Dipak kumar on destroy SplashActivityy");
        }
     }

