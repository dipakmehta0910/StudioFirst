package com.example.mehta.doctorregistration;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;
import com.example.mehta.doctorregistration.model.TestModel;
import com.example.mehta.doctorregistration.testlist.TestBaseAdapter;
import java.util.ArrayList;

public class TestListActivity extends Activity {
    private ListView mtestlist;
    ArrayList<TestModel> list=new ArrayList<TestModel>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view1);
        mtestlist=(ListView)findViewById(R.id.list_view1);
        dumytestlist();
        TestBaseAdapter testBaseAdapter=new TestBaseAdapter(this,list);
        mtestlist.setAdapter(testBaseAdapter);
    }

    public void dumytestlist(){
        TestModel testModel=new TestModel("1Jupitor Pharmacy","0.3km","#702,2nd main,At Karma","Dubai 12345");
        TestModel testMode2=new TestModel("2Jupitor Pharmacy","0.3km","#702,2nd main,At Karma","Dubai 12345");
        TestModel testMode3=new TestModel("3Jupitor Pharmacy","0.3km","#702,2nd main,At Karma","Dubai 12345");
        TestModel testMode4=new TestModel("3Jupitor Pharmacy","0.3km","#702,2nd main,At Karma","Dubai 12345");
        TestModel testMode5=new TestModel("4Jupitor Pharmacy","0.3km","#702,2nd main,At Karma","Dubai 12345");

        list.add(testModel);
        list.add(testMode2);
        list.add(testMode3);
        list.add(testMode4);
        list.add(testMode5);
    }
}
