package com.example.mehta.mobile_grid_view.model_list;

public class ModelGrid {
    private String firsttext;
    private String secondtext;

    public ModelGrid(String firsttext){
        this.firsttext=firsttext;
    }

    public  String getFirsttext(){
        return firsttext;
    }

    public String getSecondtext(){
        return secondtext;
    }
}
